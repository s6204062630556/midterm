import { Flight } from "../components/flight-form/flight"

export class Mockflight {
  public static mflights: Flight[] = [
    {
      fullName: "Veerapat Samawanich",
      from: "Bangkok",
      to: "Tokyo",
      type: "One way",
      departure: new Date('2565-03-7'),
      arrival: new Date('2565-03-9'),
      adults: 1,
      children: 0,
      infants: 0,
    }
  ]
}
