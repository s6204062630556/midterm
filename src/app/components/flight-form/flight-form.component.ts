import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PageService } from 'src/app/services/page.service';
import { Flight } from './flight';

@Component({
  selector: 'app-flight-form',
  templateUrl: './flight-form.component.html',
  styleUrls: ['./flight-form.component.css']
})
export class FlightFormComponent implements OnInit {
  flights !: Flight[];
  flightForm !: FormGroup;
  defaultMinDate!: Date;

  constructor(private fb: FormBuilder, private pageService: PageService) { }

  ngOnInit(): void {
    this.flightForm = this.fb.group({
      fullName: ['', Validators.required],
      from: ['', Validators.required],
      to: ['', Validators.required],
      type: ['', Validators.required],
      departure: ['', Validators.required],
      arrival: ['', Validators.required],
      adults: ['', [Validators.max(10), Validators.required, Validators.pattern("^[0-9]*$")]],
      children: ['', [Validators.max(10), Validators.required, Validators.pattern("^[0-9]*$")]],
      infants: ['', [Validators.max(10), Validators.required, Validators.pattern("^[0-9]*$")]]
    });
    this.defaultMinDate = new Date;
    this.getPages();
  }
  getPages() {
    this.flights = this.pageService.getPages();
  }
  onSubmit(f: Flight): void {
    const departureYear = f.departure.getFullYear() + 543;
    const arrivalYear = f.arrival.getFullYear() + 543;
    f.departure = new Date((f.departure.getMonth() + 1) + '/' + f.departure.getDate() + '/'  + departureYear);
    f.arrival = new Date((f.arrival.getMonth() + 1) + '/' + f.arrival.getDate() + '/' + arrivalYear);
    this.pageService.addFlight(f);
    this.flightForm.reset();
  }
}
