import { Injectable } from '@angular/core';
import { Flight } from '../components/flight-form/flight';
import { Mockflight } from '../share/mockflight';
@Injectable({
  providedIn: 'root'
})
export class PageService {
  flights: Flight[] =[];

  constructor() {
    this.flights = Mockflight.mflights
  }

  getPages():Flight[]{
    console.log(this.flights);
    return this.flights;
  }

  addFlight(f:Flight):void{
    this.flights.push(f);

  }
}
